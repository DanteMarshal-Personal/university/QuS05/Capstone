import os
import json5
import numpy as np
from os import path
import tensorflow as tf
from tensorflow import keras
from Data.Midi import Action
from Convert.Numpy import ImageToNumpy
from typing import List, Tuple, Callable

SW_LENGTH_INPUT: int = 128
SW_LENGTH_OUTPUT: int = SW_LENGTH_INPUT
SW_LENGTH_DATA: int = int(SW_LENGTH_INPUT + SW_LENGTH_OUTPUT)
SW_MEMORY_DEPTH: int = int(SW_LENGTH_INPUT)
# SW_LATENT_TEMPO: int = int(Action.NEURON_COUNT_TEMPO)
# SW_LATENT_TIME_SIG: int = int(Action.NEURON_COUNT_TIME_SIGNATURE)
SW_LATENT_NOTES: int = int(Action.NEURON_COUNT_NOTES)

class SongWriterDataGenerator(keras.utils.Sequence):
	lastOpenFile: str = None
	lastOpenData: np.ndarray = None
	def __init__(self, files: List[str], spansPerFile: int = 10):
		self.files = files
		self.spansPerFile = spansPerFile
		self.fileArrs: List[List[np.ndarray]] = []
	def __getitem__(self, idx: int):
		fIndex: int = int(idx / self.spansPerFile)
		while fIndex >= len(self.fileArrs):
			self.fileArrs.append([])
		fSpanIndex: int = int(idx % self.spansPerFile)
		while fSpanIndex >= len(self.fileArrs[fIndex]):
			self.fileArrs[fIndex].append(None)
		if self.fileArrs[fIndex][fSpanIndex] == None:
			# Load the Data
			if self.lastOpenData is None or self.lastOpenFile != self.files[fIndex]:
				self.lastOpenData = ImageToNumpy(self.files[fIndex])
				self.lastOpenFile = self.files[fIndex]
			dataIdx = int((self.lastOpenData.shape[0] - SW_LENGTH_DATA) * (float(fSpanIndex) / float(self.spansPerFile)))
			self.fileArrs[fIndex][fSpanIndex] = (
				self.lastOpenData[dataIdx:dataIdx+SW_LENGTH_INPUT, :],
				self.lastOpenData[dataIdx+SW_LENGTH_INPUT:dataIdx+SW_LENGTH_DATA, :]
			)
		retIn, retOut = self.fileArrs[fIndex][fSpanIndex]
		retIn, retOut = SongWriterDataGenerator.SplitFeatures(np.array([retIn]), True), SongWriterDataGenerator.SplitOutputs(retOut)
		return retIn, retOut
	def __len__(self):
		return len(self.files) * self.spansPerFile
	def LoadAllPairs(self):
		count = len(self)
		retIn, retOut = self[0]
		for i in range(1, count):
			dIn, dOut = self[i]
			for j in range(len(dIn)):
				retIn[j] = tf.concat([retIn[j], dIn[j]], axis=0).numpy()
			for j in range(len(dOut)):
				retOut[j] = tf.concat([retOut[j], dOut[j]], axis=0).numpy()
		return retIn, retOut
	@staticmethod
	def SplitFeatures(v: np.ndarray, addLstmStates: bool = False):
		ret = [
			# np.reshape(
			# 	v[:, :, :Action.NEURON_COUNT_TEMPO],
			# 	(1, v.shape[1], Action.NEURON_COUNT_TEMPO)
			# ),
			# np.reshape(
			# 	v[:, :, Action.NEURON_COUNT_TEMPO:Action.NEURON_COUNT_TEMPO+Action.NEURON_COUNT_TIME_SIGNATURE],
			# 	(1, v.shape[1], Action.NEURON_COUNT_TIME_SIGNATURE)
			# ),
			np.reshape(
				v[:, :, Action.NEURON_COUNT_TEMPO+Action.NEURON_COUNT_TIME_SIGNATURE:],
				(1, v.shape[1], Action.NEURON_COUNT_NOTES)
			)
		]
		if addLstmStates:
			ret += [
				# np.zeros((1, SW_LATENT_TEMPO)),
				# np.zeros((1, SW_LATENT_TIME_SIG)),
				np.zeros((1, SW_LATENT_NOTES))
			]
		return ret
	@staticmethod
	def SplitOutputs(v: np.ndarray):
		return [
			# np.reshape(
			# 	v[:, :Action.NEURON_COUNT_TEMPO],
			# 	(1, SW_LENGTH_OUTPUT, Action.NEURON_COUNT_TEMPO)
			# ),
			# np.reshape(
			# 	v[:, Action.NEURON_COUNT_TEMPO:Action.NEURON_COUNT_TEMPO+Action.NEURON_COUNT_TIME_SIGNATURE],
			# 	(1, SW_LENGTH_OUTPUT, Action.NEURON_COUNT_TIME_SIGNATURE)
			# ),
			np.reshape(
				v[:, Action.NEURON_COUNT_TEMPO+Action.NEURON_COUNT_TIME_SIGNATURE:],
				(1, SW_LENGTH_OUTPUT, Action.NEURON_COUNT_NOTES)
			)
		]
	@staticmethod
	def ConcatFeatures(v: List[np.ndarray]):
		vLen: int = len(v)
		if vLen > 0:
			return tf.concat(v, axis=2).numpy()
		return np.zeros((0, 0, Action.NEURON_COUNT))
	def ConcatTimes(v: List[np.ndarray]):
		vLen: int = len(v)
		if vLen > 0:
			return tf.concat(v, axis=1)
		return np.zeros((0, 0, Action.NEURON_COUNT))

def SongWriterModelCreate() -> keras.Model:
	# The Layers : IO
	tInputOfNotes = keras.Input((SW_LENGTH_INPUT, Action.NEURON_COUNT_NOTES))
	# tInputCreativity = keras.Input(1, 1)
	tOutputOfNotes = []
	# The Layers : LSTM
	lNotesLSTM = keras.layers.LSTM(SW_LATENT_NOTES, return_state=True)
	# The Layers : LSTM States
	sNotesLSTMInit = keras.layers.Input(name='sNotesLSTMInit', shape=SW_LATENT_NOTES)
	# The Layers : Densors
	lNotesDensor = keras.layers.Dense(Action.NEURON_COUNT_NOTES, activation=keras.activations.softmax)
	# The Layers : Reshape
	lNotesReshape = keras.layers.Reshape((1, Action.NEURON_COUNT_NOTES))
	# The Loop
	tNotesLSTM = sNotesLSTMCell = sNotesLSTMInit
	for i in range(SW_MEMORY_DEPTH):
		print(F"{i: 3d} / {SW_MEMORY_DEPTH: 3d}", end='\r')
		# Notes
		tNotesInput = lNotesReshape(tInputOfNotes[:, i, :])
		tNotesLSTM, _, sNotesLSTMCell = lNotesLSTM(tNotesInput, initial_state=[tNotesLSTM, sNotesLSTMCell])
		tNotesDense = lNotesDensor(tNotesLSTM)
		tNotesDense = lNotesReshape(tNotesDense)
		tOutputOfNotes.append(tNotesDense)
		# tOutputOfNotes = tNotesDense
	# Concat
	lConcat = keras.layers.Concatenate(axis=1)
	tOutputOfNotes = lConcat(tOutputOfNotes)
	# Build the Model
	ret = keras.Model(
		[ tInputOfNotes, sNotesLSTMInit ],
		[ tOutputOfNotes ]
	)
	ret.compile(loss=keras.losses.CategoricalCrossentropy(), optimizer=keras.optimizers.Adam(), metrics=[])
	return ret

def SongWriterModelLoad(dirPath: str):
	ret = keras.models.load_model(dirPath)
	ret.compile(loss=keras.losses.CategoricalCrossentropy(), optimizer=keras.optimizers.Adam(), metrics=[])
	return ret
