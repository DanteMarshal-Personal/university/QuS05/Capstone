import sys
import numpy as np
import tensorflow as tf
from typing import List, Dict, Callable
from random import randint, normalvariate
from os import listdir, path, makedirs, remove
from Convert.Midi import MidiToActions, ActionsToMidi
from Data.Midi import Action, MidiTrack, MidiState, MidiObject
from Convert.Numpy import NumpyToImage, ActionsToImage, ImageToNumpy, ImageToActions
from Data.SongWriter import SongWriterModelCreate, SongWriterModelLoad, SongWriterDataGenerator, SW_LENGTH_INPUT, SW_LENGTH_OUTPUT

class Main:
	def ParseArgs(self, args: List[str]):
		for arg in args:
			lArg = arg.lower()
			if lArg.startswith('source:'):
				self.sourcePath = arg[7:]
				self.sourceMidi.extend(Main.FindFilesIn(self.sourcePath, '.midi'))
				self.sourceVPng.extend(Main.FindFilesIn(self.sourcePath, '.png'))
			elif lArg.startswith('destin:'):
				self.destinPath = arg[7:]
			elif lArg.startswith('logfile:'):
				self.logFile = open(arg[8:], 'w')
				sys.stdout = self.logFile
			elif lArg.startswith('action:'):
				self.action = arg[7:].lower().split(':')
	def __init__(self, args):
		self.action = ['help']
		self.logFile = None
		self.sourceMidi: List[str] = []
		self.sourceVPng: List[str] = []
		self.sourcePath = 'SourceDir'
		self.destinPath = 'DestinDir'
		self.ParseArgs(args)
		self.sourceMidi.sort()
		self.sourceVPng.sort()
	def Close(self):
		if self.logFile != None:
			self.logFile.close()
		return self
	def Run(self):
		if self.action[0] == 'help':
			pass # Print Help
		elif self.action[0] == 'miditovector':
			self.RunMidiToVector()
		elif self.action[0] == 'vectortomidi':
			self.RunVectorToMidi()
		elif self.action[0] == 'train':
			self.RunTrain()
		elif self.action[0] == 'generate':
			self.RunGenerate()
		else:
			print(f"Undefined Action {self.action}")
		return self
	def RunMidiToVector(self):
		midiCount = len(self.sourceMidi)
		print(F"Converting {midiCount} Midi files to Matrices @ {self.destinPath}")
		if not path.isdir(self.destinPath):
			makedirs(self.destinPath)
		else:
			oldFiles = listdir(self.destinPath)
			if len(oldFiles) > 0:
				print("Removing old samples")
				for f in oldFiles:
					remove(path.join(self.destinPath, f))
		print("Processing samples ...", end="\r")
		for i in range(midiCount):
			print(F"Processing sample {i: 4d} / {midiCount: 4d}", end="\r")
			# Open
			theTrack = MidiObject.LoadFromFile(self.sourceMidi[i]).GetTrackByProgram('000')
			if theTrack == None:
				continue
			theTrack = theTrack.Serialize()
			# Split and Save
			NumpyToImage(theTrack, path.join(self.destinPath, F"{i:05d}.png"))
		print("Finished processing samples ...")
	def RunVectorToMidi(self):
		vPngCount = len(self.sourceVPng)
		print(F"Preparing {vPngCount} Matrices to Midi files @ {self.destinPath}")
		if path.isdir(self.destinPath):
			oldFiles = listdir(self.destinPath)
			print(F"Removing {len(oldFiles)} old samples (Previous Midi Files with same name)")
			for f in oldFiles:
				remove(path.join(self.destinPath, f))
		else:
			makedirs(self.destinPath)
		print('Processing samples ...', end='\r')
		for f in self.sourceVPng:
			fName = '.'.join(path.basename(f).split(path.extsep)[:-1])
			midiPath = path.join(self.destinPath, fName + '.midi')
			print(F"Processing samples : {f} -> {midiPath}", end='\r')
			midi = MidiObject(fName, 960)
			midi.tracks['000:Default'] = MidiTrack.Deserialize('000:Default', ImageToNumpy(f), midi.tpb)
			midi.SaveToFile(midiPath)
	def RunTrain(self):
		# self.sourceVPng = self.sourceVPng[0:20]
		print('\nCreating the model\n')
		m = SongWriterModelCreate()
		print('Training the model')
		srcFiles = []
		for i in range(len(self.sourceVPng)):
			li = int(i % SW_LENGTH_INPUT)
			while li >= len(srcFiles):
				srcFiles.append([])
			srcFiles[li].append(self.sourceVPng[i])
		srcLen = len(srcFiles)
		for i in range(srcLen):
			dGen = SongWriterDataGenerator(srcFiles[i], 25)
			print(F"Training the model {i: 3d} / {srcLen: 3d}; Including {len(srcFiles[i])} files; Converted to {len(dGen)} tensors")
			dIn, dOut = dGen.LoadAllPairs()
			dGen = None
			m.fit(dIn, dOut)
			dIn = dOut = None
		print(F"Training Finished. Saving the Model @ '{self.destinPath}'")
		m.save(self.destinPath)
	def RunGenerate(self):
		print(F"Loading Model @ '{self.sourcePath}'")
		m = SongWriterModelLoad(self.sourcePath)
		print(F"\nLoading random initial value for model '{self.sourcePath}'")
		genCount = int(self.action[1]) if len(self.action) > 1 else SW_LENGTH_OUTPUT
		genIn = np.zeros((1, SW_LENGTH_INPUT, Action.PIXELS_COUNT), dtype=float)
		genOut = np.zeros((1, 0, Action.PIXELS_COUNT), dtype=float)
		rndPath = self.sourceVPng[randint(0, len(self.sourceVPng) - 1)]
		print(F"Randomly Selected Data for Input : {rndPath}")
		rndData = ImageToNumpy(rndPath)[:, :]
		rndIndex = randint(0, rndData.shape[0] - SW_LENGTH_INPUT - 1)
		print(F"Randomly Selected Range of Input = [{rndIndex}, {rndIndex + SW_LENGTH_INPUT})")
		genIn[0, :, :] = rndData[rndIndex:rndIndex+SW_LENGTH_INPUT, :]
		rndData = rndIndex = None
		avgTempo = np.array(np.median(genIn[0, :, 0:Action.NEURON_COUNT_TEMPO], 0))
		avgTimeSig = np.array(np.median(genIn[0, :, Action.NEURON_COUNT_TEMPO:Action.NEURON_COUNT_TEMPO+Action.NEURON_COUNT_TIME_SIGNATURE], 0))
		genPrb = SongWriterDataGenerator.ConcatTimes([
			(genIn * 255).astype('uint8'),
			np.zeros((1, 1, Action.PIXELS_COUNT)),
			np.ones((1, 1, Action.PIXELS_COUNT)),
			np.zeros((1, 1, Action.PIXELS_COUNT))
		])
		print(F"Generating using the Model ...", end='\t\r')
		iteration = 0
		genRes = (genIn * 255).astype('uint8')
		while(genOut.shape[1] < genCount):
			iteration += 1
			print(F"Generating using the Model (Iter : {iteration}) ...", end='\t\r')
			# Gen
			tmpNotes = m.predict(SongWriterDataGenerator.SplitFeatures(genIn, True))
			tmpOut = self.ProcessOutputs(tmpNotes, avgTempo, avgTimeSig)
			genPrb = SongWriterDataGenerator.ConcatTimes([genPrb, tmpOut])
			genIn = SongWriterDataGenerator.ConcatTimes([genIn[:, tmpOut.shape[1]:, :], tmpOut])
			# Post-Process
			genOut = SongWriterDataGenerator.ConcatTimes([genOut, (tmpOut * 255).astype('uint8')])
		genIn = SongWriterDataGenerator.ConcatTimes([genRes, genOut])
		print(F"Generation Finished in {iteration} iterations !", end='\t\n')
		NumpyToImage(genPrb[0, :, :].numpy(), self.destinPath.replace('.png', F".Prb.png"))
		NumpyToImage(genOut[0, -genCount:, :].numpy(), self.destinPath.replace('.png', F".Out.png"))
		NumpyToImage(genIn[0, :, :].numpy(), self.destinPath.replace('.png', F".Res.png"))
	def ProcessOutputs(self, notes: np.ndarray, avgTempo: np.ndarray, avgTimeSig: np.ndarray):
		notes[notes < 0.1] = 0
		tempo = np.zeros((1, notes.shape[1], Action.NEURON_COUNT_TEMPO))
		for i in range(tempo.shape[1]):
			tempo[0, i, :] = avgTempo
		timeSig = np.zeros((1, notes.shape[1], Action.NEURON_COUNT_TIME_SIGNATURE))
		for i in range(timeSig.shape[1]):
			timeSig[0, i, :] = avgTimeSig
		return SongWriterDataGenerator.ConcatFeatures([tempo, timeSig, notes])
	def ArrJoin(self, arr, extractCreativity: bool = False) -> np.ndarray:
		ret = np.concatenate(tuple(arr[0:3]), axis=2)
		if extractCreativity:
			if len(arr) > 3:
				return (ret, arr[3])
			return (ret, None)
		return ret
	def ArrSplit(self, arr, creativity: float = None):
		ret = []
		ret.append(arr[:, :, 0:Action.NEURON_COUNT_TEMPO])
		ret.append(arr[:, :, Action.NEURON_COUNT_TEMPO:Action.NEURON_COUNT_TEMPO+Action.NEURON_COUNT_TIME_SIGNATURE])
		ret.append(arr[:, :, Action.NEURON_COUNT_TEMPO+Action.NEURON_COUNT_TIME_SIGNATURE:])
		if creativity != None:
			ret.append(creativity * np.ones((arr.shape[0], 1, 1)))
		return ret
	@staticmethod
	def FindFilesIn(dirPath: str, extension: str) -> List[str]:
		ret = []
		dirPath = path.abspath(dirPath)
		subs = listdir(dirPath)
		for s in subs:
			subPath = path.join(dirPath, s)
			if path.isfile(subPath):
				if s.lower().endswith(extension):
					ret.append(subPath)
			elif path.isdir(subPath):
				ret.extend(Main.FindFilesIn(subPath, extension))
		return ret

if __name__ == "__main__":
	args = sys.argv[1:]
	tmpArgs = []
	for a in args:
		if a.lower() == 'then':
			print(128 * '-')
			Main(tmpArgs).Run().Close()
			tmpArgs = []
		else:
			tmpArgs.append(a)
	Main(tmpArgs).Run().Close()