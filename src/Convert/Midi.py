from typing import List, Dict, Tuple
from Data.Midi import Action, MidiObject, MidiTrack, MidiState

def MidiStatesToActions(states: Dict[int, MidiState], ticksPerBeat: int) -> List[Action]:
	# print('Converting {} midi states to actions, Using TPB = {}'.format(len(states), ticksPerBeat))
	ret = []
	last = Action
	lastTick = 0
	def UpdateTime(tick: int) -> None:
		nonlocal lastTick
		if tick == lastTick:
			# Nothing to do
			return
		while lastTick < tick:
			oldAction = ret[lastTick]
			newAction = oldAction.Copy(withPressed=False, withHeld=True)
			for n in oldAction.notesPressed:
				if not n in newAction.notesHeld:
					newAction.notesHeld.append(n)
			ret.append(newAction)
			lastTick += 1
	ret.append(Action())
	timeDelta = list(states.keys())[0]
	for t in states:
		timeStep = ticksPerBeat / ((states[t].tempo / 4) / (Action.RESOLUTION * 60))
		tick = round((t - timeDelta) / timeStep)
		UpdateTime(tick)
		ret[tick].tempo = states[t].tempo
		ret[tick].timeSig = states[t].timeSig
		for (n, v) in states[t].notesOn:
			if not n in ret[tick].notesPressed:
				ret[tick].notesPressed.append(n)
			if not n in ret[tick].notesHeld:
				ret[tick].notesHeld.append(n)
		for n in states[t].notesOff:
			if n in ret[tick].notesHeld:
				ret[tick].notesHeld.remove(n)
	# print("Converted to {} actions".format(len(ret)))
	# print("\n".join(map(str, ret)))
	return ret

def MidiTrackToActions(midi: MidiTrack, ticksPerBeat: int) -> Tuple[str, List[Action]]:
	ret = (midi.name, MidiStatesToActions(midi.states, ticksPerBeat))
	# print(f"Converting Track '{midi.name}' to '{len(ret[1])}' actions")
	return ret

def MidiToActions(midi: MidiObject) -> List[Tuple[str, List[Action]]]:
	# print("Converting Object '{}' to actions".format(midi.name))
	ret = []
	for t in midi.TracksList:
		ret.append(MidiTrackToActions(t, midi.tpb))
	return ret

def ActionsToMidiStates(actions: List[Action], ticksPerBeat: int) -> Dict[int, MidiState]:
	# print("Converting {} actions to midi states, Using TPB = {}".format(len(actions), ticksPerBeat))
	time: int = 0
	lastNotesHeld:List[int] = []
	ret: Dict[int, MidiState] = {}
	lastState: MidiState = None
	def IsAnythingNew(act: Action, last: MidiState) -> bool:
		nonlocal lastNotesHeld
		if last.tempo != act.tempo or last.timeSig != act.timeSig or len(last.notesOn) != len(act.notesPressed) or len(lastNotesHeld) != len(act.notesHeld):
			return True
		for n in last.notesOn:
			if not n in act.notesPressed:
				return True
		for n in act.notesHeld:
			if not n in lastNotesHeld:
				return True
		return False
	for tick in range(0, len(actions)):
		action: Action = actions[tick]
		# Set Fields
		if lastState == None or IsAnythingNew(action, lastState):
			t = round(time)
			newState = MidiState(t)
			newState.tempo = action.tempo
			newState.timeSig = action.timeSig
			for n in action.notesPressed:
				newState.notesOn.append((n, 100))
			for n in lastNotesHeld:
				if not n in action.notesHeld:
					newState.notesOff.append(n)
			ret[t] = lastState = newState
			lastNotesHeld = action.notesHeld
		# Update Time
		timeStep = ticksPerBeat / ((action.tempo / 4) / (Action.RESOLUTION * 60))
		time += timeStep
	return ret

def ActionsToMidiTrack(acts: Tuple[str, List[Action]], ticksPerBeat: int) -> MidiTrack:
	name: str = acts[0]
	actions: List[Action] = acts[1]
	# print("Converting Actions '{}' to Midi Track".format(name))
	ret = MidiTrack(name)
	ret.states = ActionsToMidiStates(actions, ticksPerBeat)
	ret.times = list(ret.states.keys())
	ret.times.sort()
	return ret

def ActionsToMidi(name: str, acts: List[Tuple[str, List[Action]]], ticksPerBeat: int) -> MidiObject:
	# print("Converting Action Collection '{}' to Midi Object".format(name))
	ret = MidiObject(name, ticksPerBeat)
	for actList in acts:
		track = ActionsToMidiTrack(actList, ticksPerBeat)
		ret.tracks[track.name] = track
	return ret